package comiendomanzanas.pillspenser.model;

import java.io.Serializable;

public class Doze implements Serializable {

    private int year, month, dayOfMonth;
    private int currentHour, currentMinute;
    private int npills;

    public Doze(int year, int month, int dayOfMonth, int currentHour, int currentMinute, int npills) {
        this.year = year;
        this.month = month;
        this.dayOfMonth = dayOfMonth;
        this.currentHour = currentHour;
        this.currentMinute = currentMinute;
        this.npills = npills;
    }

    public Doze() {
        this.year = 2017;
        this.month = 1;
        this.dayOfMonth = 1;
        this.currentHour = 0;
        this.currentMinute = 0;
        this.npills = 1;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDayOfMonth() {
        return dayOfMonth;
    }

    public void setDayOfMonth(int dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    public int getCurrentHour() {
        return currentHour;
    }

    public void setCurrentHour(int currentHour) {
        this.currentHour = currentHour;
    }

    public int getCurrentMinute() {
        return currentMinute;
    }

    public void setCurrentMinute(int currentMinute) {
        this.currentMinute = currentMinute;
    }

    public int getNpills() {
        return npills;
    }

    public void setNpills(int npills) {
        this.npills = npills;
    }

}
