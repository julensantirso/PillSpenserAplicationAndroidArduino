package comiendomanzanas.pillspenser.httpPostGet;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rares on 10/6/17.
 */

public class GetTask extends AsyncTask<String, Void, String> {

    OkHttpClient client;

    private Exception exception;

    protected String doInBackground(String... urls) {
        try {
            String getResponse = get("http://pillspenser.esy.es/helloworld.txt");
            return getResponse;
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    protected void onPostExecute(String getResponse) {
        System.out.println(getResponse);
    }

    public String get(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}
