package comiendomanzanas.pillspenser.controller;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import comiendomanzanas.pillspenser.R;
import comiendomanzanas.pillspenser.activity.DozeListActivity;
import comiendomanzanas.pillspenser.activity.TabActivity;
import comiendomanzanas.pillspenser.activity.settings.SettingsActivity;
import comiendomanzanas.pillspenser.adapter.DozeListAdapter;
import comiendomanzanas.pillspenser.data.DataManager;
import comiendomanzanas.pillspenser.model.Doze;

public class DozeListController {

    private Activity activity;
    private DozeListAdapter recyclerAdapter;
    private ListView dozesRecyclerView;

    public DozeListController(DozeListActivity listActivity) {
        this.activity = listActivity;
    }

    public void initFloatingActionButtonEvent(FloatingActionButton fab) {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent t = new Intent(activity, TabActivity.class);
                activity.startActivity(t);
            }
        });
    }

    public void initList(ListView dozesRecyclerView, Context c) {
        ArrayList<Doze> dozeList = DataManager.getInstance().readDozeList(activity);
        this.dozesRecyclerView = dozesRecyclerView;

        this.dozesRecyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                DataManager manager = DataManager.getInstance();
                manager.setLastViewedDoze(manager.getDozeAt(position));
                manager.setLastViewedDozePosition(position);
            }
        });

        this.dozesRecyclerView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                DataManager manager = DataManager.getInstance();
                manager.setLastViewedDoze(manager.getDozeAt(position));
                manager.setLastViewedDozePosition(position);
                showChooserDialog(position);
                return true;
            }
        });

        recyclerAdapter = new DozeListAdapter(getActivity(), dozeList);
        dozesRecyclerView.setAdapter(recyclerAdapter);
        DataManager.getInstance().setDozeListAdapter(recyclerAdapter);
    }

    public void reloadRecyclerView() {
        recyclerAdapter.notifyDataSetChanged();
    }

    private void showChooserDialog(final int position) {
        final CharSequence[] items = {"Delete"};
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Doze");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(items[0]))
                    deleteDoze(position);
            }
        });
        builder.show();
    }

    private void deleteDoze(int position) {
        DataManager.getInstance().deleteDoze(getActivity(), position);
        recyclerAdapter.notifyDataSetChanged();
    }

    public void filterByYear(int yearToFilter) {
        ArrayList<Doze> list = DataManager.getInstance().getDozesByDate(yearToFilter);
        if (list.size() > 0) {
            recyclerAdapter = new DozeListAdapter(getActivity(), list);
            dozesRecyclerView.setAdapter(recyclerAdapter);
            DataManager.getInstance().setDozeListAdapter(recyclerAdapter);
            recyclerAdapter.notifyDataSetChanged();
        } else {
            Toast.makeText(getActivity(), "No results found", Toast.LENGTH_SHORT).show();
        }
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public void showSettings() {
        Intent t = new Intent(getActivity(), SettingsActivity.class);
        getActivity().startActivity(t);
    }

    public void showAbout() {
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.diaog_about_body)
                .setTitle(R.string.dialog_about_title)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void search(final OnSearchClickEvent event) {
        //https://stackoverflow.com/questions/10903754/input-text-dialog-android
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.dialog_title_search_plac);

        // Set up the input
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String name = input.getText().toString();
                event.onNewSearch(name);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }
}
