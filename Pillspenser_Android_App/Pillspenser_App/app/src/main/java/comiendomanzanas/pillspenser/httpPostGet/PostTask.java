package comiendomanzanas.pillspenser.httpPostGet;

import android.os.AsyncTask;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by rares on 10/6/17.
 */

public class PostTask extends AsyncTask<String, Void, String> {

    OkHttpClient client;

    private Exception exception;
    MediaType JSON;

    private int npills;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;

    public PostTask(int npills, int year, int month, int day, int hour, int minute) {
        client = new OkHttpClient();
        this.npills = npills;
        this.year = year;
        this.month = month;
        this.day = day;
        this.hour = hour;
        this.minute = minute;
    }

    protected String doInBackground(String... urls) {
        try {
            String getResponse = post("http://pillspenser.esy.es/add.php", bowlingJson(npills, year, month, day, hour, minute));
            return getResponse;
        } catch (Exception e) {
            this.exception = e;
            return null;
        }
    }

    protected void onPostExecute(String getResponse) {
        System.out.println(getResponse);
    }

    private String post(String url, String urlBody) throws IOException {
        Request request = new Request.Builder()
                .url(url + urlBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            if (response != null) {
                if (response.body() != null) {
                    return response.body().string();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String bowlingJson(int numPills, int year, int month, int day, int hour, int minute) {
        return "?pills=" + numPills +
                "&year=" + year +
                "&month=" + month +
                "&day=" + day +
                "&hour=" + hour +
                "&minute=" + minute;
    }
}

