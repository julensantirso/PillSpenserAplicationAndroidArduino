/*
package comiendomanzanas.pillspenser.util;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import comiendomanzanas.pillspenser.R;
import comiendomanzanas.pillspenser.data.DataManager;
import comiendomanzanas.pillspenser.model.Doze;

public class SimpleWidgetProvider extends AppWidgetProvider {

    private static final String REFRESH_ACTION = "android.appwidget.action.APPWIDGET_UPDATE";

    private static void updateWidgetValues(AppWidgetManager appWidgetManager, Context context, RemoteViews remoteViews) {

        Intent inet = new Intent(context, PlaceDetailsActivity.class);
        inet.putExtra("widget", true);
        inet.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pend = PendingIntent.getActivity(context, 2, inet, PendingIntent.FLAG_ONE_SHOT);
        remoteViews.setOnClickPendingIntent(R.id.widgetBtn, pend);


        Doze nearest = DataManager.getInstance().getNearestDoze();
        if(nearest!=null){
            remoteViews.setTextViewText(R.id.textViewWidgetPlaceTitle, ""+nearest.getYear());
        }
        else{
            remoteViews.setTextViewText(R.id.textViewWidgetPlaceTitle, "UNKNOWN");
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        final int count = appWidgetIds.length;
        for (int i = 0; i < count; i++) {
            int widgetId = appWidgetIds[i];
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.simple_widget);

            updateWidgetValues(appWidgetManager, context, remoteViews);
            openActivityFromWidget(context);
        }
    }

    private void openActivityFromWidget(Context context) {
        Intent intent = new Intent(context, PlaceDetailsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
        RemoteViews views = new RemoteViews(context.getPackageName(),R.layout.simple_widget);
        views.setOnClickPendingIntent(R.id.widgetBtn, pendingIntent);
    }

    @Override
    public void onReceive(final Context context, Intent intent) {
        final String action = intent.getAction();

        if (action.equals(REFRESH_ACTION)) {
            AppWidgetManager mgr = AppWidgetManager.getInstance(context);
            ComponentName cn = new ComponentName(context, SimpleWidgetProvider.class);
            mgr.notifyAppWidgetViewDataChanged(mgr.getAppWidgetIds(cn), R.id.widgetLayout);
        }
        super.onReceive(context, intent);
    }

    public static void onDemandWidgetRefreshRequest(Context context) {
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.simple_widget);
        ComponentName thisWidget = new ComponentName(context, SimpleWidgetProvider.class);
        updateWidgetValues(appWidgetManager, context, remoteViews);
        appWidgetManager.updateAppWidget(thisWidget, remoteViews);
    }
}
*/
