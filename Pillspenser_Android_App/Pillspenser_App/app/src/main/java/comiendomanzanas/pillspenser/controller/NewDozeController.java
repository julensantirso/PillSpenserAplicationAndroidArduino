package comiendomanzanas.pillspenser.controller;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import comiendomanzanas.pillspenser.data.DataManager;
import comiendomanzanas.pillspenser.model.Doze;

public class NewDozeController {

    private Activity activity;

    public NewDozeController(Activity activity) {
        this.activity = activity;
    }

    public void save(Doze p) {
        saveNewDoze(p);
        getActivity().finish();
    }

    private void saveNewDoze(Doze p) {
        DataManager.getInstance().addDoze(getActivity(), p);
    }

    public boolean isValidDoze(Doze p) {
        if (p.getYear() < 2017) {
            showError("Incorrect Date");
            return false;
        } else if (p.getMonth() < 1 || p.getMonth() > 12) {
            showError("Incorrect Month");
            return false;
        } else if (p.getDayOfMonth() < 1 || p.getDayOfMonth() > 31) {
            showError("Incorrect Day");
            return false;
        } else if (p.getCurrentHour() < 0 || p.getCurrentHour() > 24) {
            showError("Incorrect Hour-Time");
            return false;
        } else if (p.getCurrentMinute() < 0 || p.getCurrentMinute() > 59) {
            showError("Incorrect Minute-Time");
            return false;
        } else if (p.getNpills() < 1) {
            showError("Incorrect number of pills");
            return false;
        }
        return true;
    }

    private void showError(String message) {
        Snackbar pop = Snackbar.make(getRootView(), message, Snackbar.LENGTH_LONG);
        pop.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(getClass().getSimpleName(), "clicked");
            }
        });
        pop.show();
    }

    public void updateAndSave(Activity activity, int position, Doze p) {
        DataManager.getInstance().replaceAt(activity, position, p);
        getActivity().finish();
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public View getRootView() {
        return getActivity().findViewById(android.R.id.content);
    }
}
