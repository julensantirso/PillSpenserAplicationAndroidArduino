package comiendomanzanas.pillspenser.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import comiendomanzanas.pillspenser.R;
import comiendomanzanas.pillspenser.model.Doze;

public class DozeListAdapter extends ArrayAdapter<Doze> {

    public DozeListAdapter(Context context, List<Doze> list) {
        super(context, R.layout.item_recyclerview_place, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Doze doze = getItem(position);
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview_place, parent, false);
        TextView placeName = (TextView) itemView.findViewById(R.id.itemPlaceNameTextView);
        TextView placeLocation = (TextView) itemView.findViewById(R.id.itemPlaceLocationTextView);
        TextView placeContactNumber = (TextView) itemView.findViewById(R.id.itemPlaceContactNumberTextView);
        placeName.setText("" + doze.getYear() + " - " + doze.getMonth() + " - " + doze.getDayOfMonth());
        placeLocation.setText("" + doze.getCurrentHour() + " : " + doze.getCurrentMinute());
        placeContactNumber.setText("Pills: " + doze.getNpills());
        return itemView;
    }


}
