package comiendomanzanas.pillspenser.activity;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import comiendomanzanas.pillspenser.R;
import comiendomanzanas.pillspenser.controller.DozeListController;
import comiendomanzanas.pillspenser.controller.OnSearchClickEvent;
import comiendomanzanas.pillspenser.data.DataManager;

public class DozeListActivity extends AppCompatActivity {

    protected DozeListController controller;
    private static final int GPS_PERMISSION_REQUEST = 0;
    private Toolbar toolbar;
    private FloatingActionButton fab;
    private ListView dozesRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doze_list);

        checkPermissions();

        fab = (FloatingActionButton) findViewById(R.id.fab);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        dozesRecyclerView = (ListView) findViewById(R.id.placesRecyclerView);

        setSupportActionBar(toolbar);

        controller = new DozeListController(this);
        getThisController().initFloatingActionButtonEvent(fab);
        getThisController().initList(dozesRecyclerView, this);
    }

    private void checkPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
            } else {
                ActivityCompat.requestPermissions(
                        this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                        GPS_PERMISSION_REQUEST);
            }
        } else {
            //DataManager.getInstance().enabledServices(this);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GPS_PERMISSION_REQUEST: {
                if (grantResults.length == 2
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    //DataManager.getInstance().enabledServices(this);
                } else {
                    Toast.makeText(getActivity(), "Location features disabled", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_with_search, menu);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        getThisController().reloadRecyclerView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_item_search:
                getThisController().search(new OnSearchClickEvent() {
                    @Override
                    public void onNewSearch(Object o) {
                        int yearToFilter = (int) o;
                        Toast.makeText(getActivity(), "" + yearToFilter, Toast.LENGTH_SHORT).show();
                        getThisController().filterByYear(yearToFilter);
                    }
                });
                return true;
            case R.id.menu_item_settings:
                controller.showSettings();
                return true;
            case R.id.menu_item_about:
                controller.showAbout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private DozeListController getThisController() {
        return controller;
    }

    protected Activity getActivity() {
        return this;
    }
}
