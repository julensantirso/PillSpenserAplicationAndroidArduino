package comiendomanzanas.pillspenser.controller;

public interface OnSearchClickEvent {

    void onNewSearch(Object o);
}
