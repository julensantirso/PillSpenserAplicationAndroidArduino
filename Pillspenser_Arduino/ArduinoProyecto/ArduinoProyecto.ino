#include <MemoryFree.h>
#include <b64.h>
#include <HttpClient.h>
#include <Servo.h>
#include <EEPROM.h>
#include <SPI.h>
#include <WiFi.h>
#define CONTENT_MAX_LENGTH 300
//WIFI
//CONFIGURACION DEL WIFI

char ssid[] = "julen";      //  NOMBRE DE LA RED SSID
char pass[] = "julenclase";   // password de la red
int keyIndex = 0;  

int status = WL_IDLE_STATUS;

// Inicializamos la libreria del cliente de wifi
WiFiClient client;

const char server[] = "http://pillspenser.esy.es";
const char path[] = "/arduino.php";

unsigned long lastConnectionTime = 0;           
const unsigned long postingInterval = 10L * 1000L; 
const int serverTimeout = 30*1000;
const int serverDelay = 1000;
char responseContent[CONTENT_MAX_LENGTH]="";

//CONFIGURACION DEL SISTEMA DE ALARMA
int speakerPin = 9;
int length = 2; // the number of notes
char notes[] = "cg "; // a space represents a rest
int beats[] = { 1, 1 };
int tempo = 80;

//VARIABLES
//ENTRADAS Y SALIDAS
const int RED_LED_PIN = 2;
const int GREEN_LED_PIN = 3 ;
const int BLUE_LED_PIN = 8;
const int sensorPin = A0; 

// VALORES
int greenIntensity = 255;
int sensorValue = 0;         // valor del sensor de luz
int sensorMin = 750;        // valor minimo del sensor de luz
int sensorMax = 10;  
Servo servoMotor;

//FUNCIONES PARA EL SONIDO DE ALARMA
void playTone(int tone, int duration) {
  for (long i = 0; i < duration * 1000L; i += tone * 2) {
    digitalWrite(speakerPin, HIGH);
    delayMicroseconds(tone);
    digitalWrite(speakerPin, LOW);
    delayMicroseconds(tone);
  }
}

void playNote(char note, int duration) {
  char names[] = { 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C' };
  int tones[] = { 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956 };
  
// play the tone corresponding to the note name
  for (int i = 0; i < 8; i++) {
    if (names[i] == note) {
      playTone(tones[i], duration);
    }
  }
}
 
void setup() {

Serial.begin(9600);
servoMotor.attach(6); 
pinMode(RED_LED_PIN, OUTPUT);
pinMode(GREEN_LED_PIN, OUTPUT);
pinMode(BLUE_LED_PIN, OUTPUT);
pinMode(speakerPin, OUTPUT);

// Calibramos el sensor en los primeros 5 segundos
while (millis() < 5000) {
  sensorValue = analogRead(sensorPin);

  //Maxima del sensor de luz
  if (sensorValue > sensorMax) {
    sensorMax = sensorValue;
  }

  // Minima del sensor de luz
  if (sensorValue < sensorMin) {
    sensorMin = sensorValue;
  }
}
  
//CONFIGURACION WIFI PARA LA CONEXION
//Si no esta connectado el serial connectar
while (!Serial) {
  ; 
}

// Comprobar que el shield este presente
if (WiFi.status() == WL_NO_SHIELD) {
  Serial.println("WiFi shield not present");
  while (true);
}

//Comprobar la version del wifi
String fv = WiFi.firmwareVersion();
if (fv != "1.1.0") {
  Serial.println("Please upgrade the firmware");
}

// Intento de conexion a la red
while (status != WL_CONNECTED) {
  Serial.print("Attempting to connect to SSID: ");
  Serial.println(ssid);
  status = WiFi.begin(ssid, pass);
}

// Escribe el estado de la conexion al wifi
printWifiStatus();
//FIN DE LA CONFIGURACION DEL WIFI
}

void loop() {
  
////WIFI
if ( status != WL_CONNECTED) { 
 // Mientras el cliente no sea aceptado, escribir el cliente.
  while (client.available()) {
    char c = client.read();
    Serial.write(c);
  }
  
  // Si no se connecta en cierto tiempo, volver a intentar concetarse.
  if (millis() - lastConnectionTime > postingInterval) {
    httpRequest();
  }
}
////FIN DEL WIFI

analogWrite(GREEN_LED_PIN, 255); 
digitalWrite(RED_LED_PIN, LOW); 
digitalWrite(BLUE_LED_PIN, LOW);
servoMotor.write(0);

getData();
//if (servicio)
//{
analogWrite(GREEN_LED_PIN, 255);  
delay(1000);                      
analogWrite(GREEN_LED_PIN, 0);   
delay(1000); 

for (int i = 0; i < length; i++) {
    if (notes[i] == ' ') {
      delay(beats[i] * tempo);
    } else {
      playNote(notes[i], beats[i] * tempo);
    }
    // pause between notes
    delay(tempo / 2); 
}
sensorValue = analogRead(sensorPin);
Serial.print("Boton: ");
Serial.println(sensorValue);

if (sensorValue > 700) {
    digitalWrite(RED_LED_PIN, HIGH); 
    analogWrite(GREEN_LED_PIN, 0);
    for (int i = 0; i < 3; i++){        
      servoMotor.write(90);
      delay(1000); 
      servoMotor.write(0);
      delay(1000); 
    }
    analogWrite(GREEN_LED_PIN, 127);
    digitalWrite(RED_LED_PIN, HIGH);
    
    int sensorPressValue = analogRead(A4);
    Serial.print("Presion: ");
    Serial.println(sensorPressValue);
    delay(1000);
    while (sensorPressValue > 300){
      sensorPressValue = analogRead(A4);
      Serial.print("Presion bucle: ");
      Serial.println(sensorPressValue);
      delay(1000);
      analogWrite(GREEN_LED_PIN, 127);
      digitalWrite(RED_LED_PIN, HIGH);
    }
    Serial.print("Presion: ");
    Serial.println(sensorPressValue);
    if (sensorPressValue < 100){
      analogWrite(GREEN_LED_PIN, 255); 
      digitalWrite(RED_LED_PIN, LOW);
      float pastillas=3;
      httpRequestPills(pastillas);
    }
  }
}
  

//FUNCIONES

// Esta funcion sirve para hacer la conexion con el servidor
void httpRequest() {
  client.stop();

  // Si la conexion es setisfactoria
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    client.println("GET /latest.txt HTTP/1.1");
    client.println("Host: www.arduino.cc");
    client.println("User-Agent: ArduinoWiFi/1.1");
    client.println("Connection: close");
    client.println();
    lastConnectionTime = millis();
  } else {
    // Si la conexion es fallida
    Serial.println("connection failed");
  }
}


//Esta funcion es para sacar por pantalla el resultado del estado del wifi

void printWifiStatus() {
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

// Funcion de envio de los datos del sensor del temperatura a traves de una conexion HTTP

void httpRequestPills(float pastillas) {
  if (client.connect(server, 80)) {
    Serial.println("connecting...");
    client.print("GET /arduino/add.php?device_id=3&data_name=pastillas&data_value=");
    client.print(pastillas);
    client.println(" HTTP/1.1");
    client.println("Host: json.internetdelascosas.es");
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: close");
    client.println();
  } 
  else {
    // if you couldn't make a connection:
    Serial.println("connection failed");
    Serial.println("disconnecting.");
    client.stop();
  }
}

void getData() {
  int error;
  WiFiClient c;
  HttpClient http(c);
  memset(responseContent, 0, CONTENT_MAX_LENGTH);
  error = http.get(server,path);
  if (error == 0) {
    Serial.println(F("startedRequest ok"));

    error = http.responseStatusCode();
    if (error >= 0) {
      Serial.print(F("Got status code: "));
      Serial.println(error);

      error = http.skipResponseHeaders();
      if (error >= 0) {
        int bodyLen = http.contentLength();
        Serial.print(F("Content length is: "));
        Serial.println(bodyLen);
        Serial.println();
        Serial.println(F("Body returned follows:"));
        
        unsigned long timeoutStart = millis();
        char c;
        while ( (http.connected() || http.available()) &&
               ((millis() - timeoutStart) < serverTimeout) ) {
            if (http.available()) {
                c = http.read();

                int lastPos = strlen(responseContent);
                responseContent[lastPos] = c;
                responseContent[++lastPos] = '\0';
               
                bodyLen--;
                timeoutStart = millis();
            } else {
                delay(serverDelay);
            }
        }
        
        Serial.println(responseContent);
      }
      else {
        Serial.print(F("Failed to skip response headers: "));
        Serial.println(error);
      }
    } else {    
      Serial.print(F("Getting response failed: "));
      Serial.println(error);
    }
  } else {
    Serial.print(F("Connect failed: "));
    Serial.println(error);
  }
  
  http.stop();
}
