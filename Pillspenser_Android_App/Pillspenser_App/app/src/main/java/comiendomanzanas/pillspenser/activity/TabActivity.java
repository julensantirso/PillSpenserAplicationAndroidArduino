package comiendomanzanas.pillspenser.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.TimePicker;

import comiendomanzanas.pillspenser.R;
import comiendomanzanas.pillspenser.controller.NewDozeController;
import comiendomanzanas.pillspenser.data.DataManager;
import comiendomanzanas.pillspenser.httpPostGet.PostTask;
import comiendomanzanas.pillspenser.model.Doze;

public class TabActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;


    private Doze d;
    protected NewDozeController controller;
    private boolean editMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                SaveDoze();
            }
        });

        controller = new NewDozeController(this);

        d = new Doze();
        editMode = getFromExtras("editmode", false);

        if (editMode) {
            d = DataManager.getInstance().getLastViewedDoze();
            DatePicker datePicker = (DatePicker) findViewById(R.id.date_picker);
            datePicker.init(d.getYear(), d.getMonth(), d.getDayOfMonth(), null);
            TimePicker timePicker = (TimePicker) findViewById(R.id.time_picker);
            timePicker.setCurrentHour(d.getCurrentHour());
            timePicker.setCurrentMinute(d.getCurrentMinute());

            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle("Edit Doze");
            }
        }
    }

    private void SaveDoze() {
        DatePicker datePicker = (DatePicker) findViewById(R.id.date_picker);
        TimePicker timePicker = (TimePicker) findViewById(R.id.time_picker);
        NumberPicker numberPicker = (NumberPicker) findViewById(R.id.numberPicker1);

        d.setYear(datePicker.getYear());
        d.setMonth(datePicker.getMonth());
        d.setDayOfMonth(datePicker.getDayOfMonth());
        d.setCurrentHour(timePicker.getCurrentHour());
        d.setCurrentMinute(timePicker.getCurrentMinute());
        d.setNpills(3); //Solucionar error number picker

        if (getThisController().isValidDoze(d)) {
            if (editMode) {
                int position = DataManager.getInstance().getLastViewedDozePosition();
                getThisController().updateAndSave(getActivity(), position, d);
            } else {
                getThisController().save(d);
            }
        }

        PostTask task = new PostTask(d.getNpills(), d.getYear(), d.getMonth(), d.getDayOfMonth(), d.getCurrentHour(), d.getCurrentMinute());
        task.execute();
    }

    private NewDozeController getThisController() {
        return (NewDozeController) controller;
    }

    protected boolean getFromExtras(String id, boolean defaultVal) {
        Intent t = getIntent();
        if (t != null) {
            Bundle extras = t.getExtras();
            if (extras != null) {
                return t.getBooleanExtra(id, defaultVal);
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_tab, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    protected Activity getActivity() {
        return this;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            int layout = -1;
            switch (sectionNumber) {
                case 0:
                    layout = R.layout.datepicker_tab;
                    break;
                case 1:
                    layout = R.layout.timepicker_tab;
                    break;
                case 2:
                    layout = R.layout.numberpicker_tab;
                    break;
            }
            args.putInt("layout", layout);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            int layout = getArguments().getInt("layout");
            View rootView = inflater.inflate(layout, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            //textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            if (layout == R.layout.numberpicker_tab) {
                final NumberPicker np = (NumberPicker) rootView.findViewById(R.id.numberPicker1);
                np.setMaxValue(100); // max value 100
                np.setMinValue(1);   // min value 0
                np.setWrapSelectorWheel(false);
            }
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return PlaceholderFragment.newInstance(position);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "SECTION 1";
                case 1:
                    return "SECTION 2";
                case 2:
                    return "SECTION 3";
            }
            return null;
        }
    }
}
