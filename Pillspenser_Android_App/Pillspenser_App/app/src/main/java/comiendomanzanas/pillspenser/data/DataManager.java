package comiendomanzanas.pillspenser.data;

import android.app.Activity;
import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import comiendomanzanas.pillspenser.adapter.DozeListAdapter;
import comiendomanzanas.pillspenser.model.Doze;
//import comiendomanzanas.pillspenser.util.BGService;

public class DataManager {
    private static final DataManager ourInstance = new DataManager();
    private Doze lastViewedDoze;
    private int lastViewedDozePosition;
    private DozeListAdapter dozeListAdapter;

    public static DataManager getInstance() {
        return ourInstance;
    }

    private ArrayList<Doze> dozeList;

    private DataManager() {
        dozeList = new ArrayList<>();
    }

    public void addDoze(Context c, Doze p) {
        this.dozeList.add(p);
        saveDozeList(c);
    }

    public void saveDozeList(Context context) {
        try {
            File f = new File(context.getFilesDir(), "dozes.data");
            ObjectOutputStream fos = new ObjectOutputStream(new FileOutputStream(f));
            fos.writeObject(this.dozeList);
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList readDozeList(Context context) {
        try {
            File f = new File(context.getFilesDir(), "dozes.data");
            ObjectInputStream fos = new ObjectInputStream(new FileInputStream(f));
            try {
                this.dozeList = (ArrayList) fos.readObject();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this.dozeList;
    }

    /*public ArrayList<Doze> getDozeList(){
        return this.dozeList;
    }*/

    public void setLastViewedDoze(Doze lastViewedDoze) {
        this.lastViewedDoze = lastViewedDoze;
    }

    public Doze getLastViewedDoze() {
        return lastViewedDoze;
    }

    public Doze getDozeAt(int position) {
        return this.dozeList.get(position);
    }

    public void deleteDoze(Activity activity, int position) {
        this.dozeList.remove(position);
        this.saveDozeList(activity);
    }

    public void replaceAt(Activity activity, int position, Doze p) {
        this.dozeList.remove(position);
        this.dozeList.add(position, p);
        this.saveDozeList(activity);
    }

    public int getLastViewedDozePosition() {
        return lastViewedDozePosition;
    }

    public void setLastViewedDozePosition(int lastViewedDozePosition) {
        this.lastViewedDozePosition = lastViewedDozePosition;
    }

    public ArrayList<Doze> getDozesByDate(int yearToFilter) {
        if (yearToFilter < 2017) {
            return this.dozeList;
        } else {
            ArrayList<Doze> matches = new ArrayList<>();
            for (Doze d : dozeList) {
                if (d.getYear() == yearToFilter) {
                    matches.add(d);
                }
            }
            return matches;
        }
    }

    /*public void enabledServices(final Context c) {
        c.startService(new Intent(c, BGService.class));
    }*/

    public void setDozeListAdapter(DozeListAdapter dozeListAdapter) {
        this.dozeListAdapter = dozeListAdapter;
    }

}
